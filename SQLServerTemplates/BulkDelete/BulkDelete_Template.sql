/****************************************************************************
=====================================
Name: Bulk Delete template
Author: Andy Seylar
Date: 10/25/2019
Desc: Template for deleting all data from a table with or without a WHERE clause
	ALWAYS REVIEW A SCRIPT BEFORE EXECUTING
=====================================

How to use this template:
1) To populate this template, press CTRL + SHIFT + M on your keyboard.
2) Input the values according to your needs.
3) Once you press 'OK', the SQL Script below will be automatically populated.
4) Notes:
	a) The values displayed in the input window are examples of how the values should be formatted.
		(i.e., if your table belongs to a specific schema, then specify that as shown)
	b) Regarding the "deleteTopNumberOfRowsDefault" option, if you do not know what to input then
		please utilize the default value.
	c) The WHERE clause - if you do not need a WHERE clause, then make the value blank.  If you do need 
		one, then make sure you include the literal "WHERE" as in the default value.
5) Once you have entered your values, review the script and then execute.  
6) If you have multiple tables to delete from, you will need to either close (without saving) and reopen this template
	or undo all the changes using CTRL + Z on your keyboard.

****************************************************************************/

USE <DatabaseName, sysname, Sandbox>

SET NOCOUNT ON 

DECLARE @RowCount int = 1, @TotalDeletedCount int = 0, @RowsExpectedToDelete int, @Msg varchar(200)

SET @RowsExpectedToDelete = (SELECT COUNT(*) FROM <TableName, sysname, dbo.BigTable> <YourFullWhereClauseHere, varchar(500), WHERE SomeField = SomeThing AND AnotherField = AnotherThing>)

select @@FETCH_STATUS

WHILE(@RowCount > 0)
BEGIN
	DELETE TOP(<DeleteTopNumberOfRowsDefault, int, 10000>)
	FROM <TableName, sysname, dbo.BigTable>
	<YourFullWhereClauseHere, varchar(500), WHERE SomeField = SomeThing AND AnotherField = AnotherThing>

	SET @RowCount = @@ROWCOUNT
	SET @TotalDeletedCount = @TotalDeletedCount + @RowCount
	SET @Msg = FORMAT(getdate(), 'dd/MM/yyyy, hh:mm:ss ') + ' | Total Deleted: ' + CAST(@TotalDeletedCount as varchar(20))  + ' / ' + CAST(@RowsExpectedToDelete as varchar(20))

	RAISERROR(@Msg, 0, 1) WITH NOWAIT

	WAITFOR DELAY '00:00:02'
END
