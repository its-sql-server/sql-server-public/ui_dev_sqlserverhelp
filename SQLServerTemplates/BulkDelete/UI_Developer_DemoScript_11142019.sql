﻿
/*
	create table dbo.BigTable (
		ID int identity (1,1),
		SomeVarcharColumn varchar(100),
	)

	C:\_Files\SQLServer\Databases

	SET FULL RECOVERY MODEL 
*/

/*************************************************/
--TABLE COUNT
	select COUNT(*) from dbo.BigTable
	select top 10 * from dbo.BigTable order by ID desc









/*************************************************/
--TABLE INSERT
	set nocount on
	declare @i int, @c int
	set @i = 500000
	set @c = 0

	while(@c < @i)
	begin
		insert into dbo.BigTable (SomeVarcharColumn) values ('Hello UI Developers!!!')
		insert into dbo.BigTable (SomeVarcharColumn) values ('This is a big table!!!')
		set @c += 1
		--print(cast(@c as varchar(6)))
	end

--Full vs simiple recovery example
	SELECT name, recovery_model_desc FROM sys.databases WHERE name = 'Sandbox'

	USE [master]
	ALTER DATABASE [Sandbox] SET RECOVERY FULL  
	--ALTER DATABASE [Sandbox] SET RECOVERY SIMPLE
	USE [Sandbox]
	
--Note table locking
	use [Sandbox]
	select COUNT(*) from dbo.BigTable








/*************************************************/
--BAD DELETE EXAMPLE 
	--truncate table dbo.BigTable

	begin transaction
	delete from dbo.BigTable
	where SomeVarcharColumn = 'This is a big table!!!'
	rollback transaction











/*************************************************/
--SHRINK FILE
	USE [Sandbox]
	GO
	DBCC SHRINKFILE (N'Sandbox_log' , 0, TRUNCATEONLY)
	GO

