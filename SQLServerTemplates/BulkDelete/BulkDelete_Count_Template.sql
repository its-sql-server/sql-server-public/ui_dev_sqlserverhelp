/****************************************************************************
=====================================
Name: Bulk Delete SELECT Count template
Author: Andy Seylar
Date: 11/11/2019
Desc: Template for viewing the number of rows to be deleted
	ALWAYS REVIEW A SCRIPT BEFORE EXECUTING
=====================================

How to use this template:
1) To populate this template, press CTRL + SHIFT + M on your keyboard.
2) Input the values according to your needs.
3) Once you press 'OK', the SQL Script below will be automatically populated.
4) Notes:
	a) You will be provided the number of rows to be deleted based on your WHERE clause.
	b) This will be the number of rows deleted with the BulkDelete_Template.sql script.  
	c) You can use this SELECT statement to review if you would even need to utilize the BulkDelete_Template.sql script.
		A good rule of thumb is anything over 10,000 rows.  That is, if you are going to delete more than 10,000 rows then
		you should utilize the bulk delete script.
5) Once you have entered your values, review the script and then execute.  
6) If you have multiple tables to delete from, you will need to either close (without saving) and reopen this template
	or undo all the changes using CTRL + Z on your keyboard.

****************************************************************************/

USE <DatabaseName, sysname, Sandbox>

SELECT COUNT(*) AS [#RowsToBeDeleted]
FROM <TableName, sysname, dbo.BigTable>
<YourFullWhereClauseHere, varchar(500), WHERE SomeField = SomeThing AND AnotherField = AnotherThing>
